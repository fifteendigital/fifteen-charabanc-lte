<?php 

namespace Fifteen\CharabancLte;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class CharabancLteServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }
    
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishPublic();
        $this->publishAssets();
        $this->publishViews();
        $this->publishGeneratorTemplates();
        $this->publishGeneratorTemplateConfig();
    }
    
    /**
     * Publish public resource assets to Laravel project
     *
     * @return void
     */
    private function publishPublic()
    {
        $this->publishes([
            __DIR__ . '/../www/public/build' => public_path('build'),
        ], 'public');
    }

    /**
     * Publish assets to Laravel project
     *
     * @return void
     */
    private function publishAssets()
    {
        $this->publishes([
            __DIR__ . '/../www/resources/assets/vendor/lte.zip' => base_path('resources/assets/vendor/lte.zip'),
        ], 'assets');
    }

    /**
     * Publish views to Laravel project
     *
     * @return void
     */
    private function publishViews()
    {
        $this->publishes([
            __DIR__ . '/../www/resources/views/templates/lte' => base_path('resources/views/templates/lte'),
            __DIR__ . '/../www/resources/views/screens' => base_path('resources/views/screens'),
            __DIR__ . '/../www/resources/views/errors' => base_path('resources/views/errors'),
        ], 'views');
    }

    /**
     * Publish generator templates to Laravel project
     *
     * @return void
     */
    private function publishGeneratorTemplates()
    {
        $this->publishes([
            __DIR__ . '/../www/resources/generators' => base_path('resources/generators'),
        ], 'generators');
    }

    /**
     * Publish generator template config to Laravel project
     *
     * @return void
     */
    private function publishGeneratorTemplateConfig()
    {
        $this->publishes([

            __DIR__ . '/../www/config/fifteen-charabanc.php' => config_path('fifteen-charabanc.php'),
            __DIR__ . '/../www/config/fifteen-generators.php' => config_path('fifteen-generators.php'),

        ], 'config'); 
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['fifteen.charabanc-lte'];
    }

}