# Charabanc-LTE

###Installation

Add Charabanc-LTE to your composer.json file:

    "require": {
        "php": ">=5.5.9",
        "laravel/framework": "5.2.*",
        "fifteen/charabanc": "1.1.*",
        "fifteen/charabanc-lte": "1.0.*"
    },

Add minimum-stability: dev (IMPORTANT!), and as the repository isn't on Packagist, you also need to include it in the repositories list:

```sh
    "minimum-stability": "dev",
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:fifteendigital/fifteen-charabanc-lte.git"
        }
    ]
```

Run composer update:

```sh
$ composer update
```

Register service provider in config/app.php:

```php
    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        ...

        /*
         * Package Service Providers...
         */
        Fifteen\CharabancLte\CharabancLteServiceProvider::class,
    ];
```

Publish files to application:

```sh
$ php artisan vendor:publish --provider="Fifteen\CharabancLte\CharabancLteServiceProvider" --force
```

The resources/assets folder (LTE) is zipped up - this needs to be unzipped:

```sh
$ unzip resources/assets/vendor/lte.zip -d resources/assets/vendor && rm -f resources/assets/vendor/lte.zip
```

Add the following to gulpfile.js, inside elixir():

```js
    // lte
    mix
      .sass([
          // base template
          '../vendor/bootstrap/css/bootstrap.css', 
          '../vendor/font-awesome/css/font-awesome.min.css',
          '../vendor/lte/AdminLTE.css',
          '../vendor/lte/skins/skin-black-light.css',
          '../vendor/lte/plugins/iCheck/square/blue.css', 
          '../vendor/bootstrap-datepicker/css/datepicker.css',
          '../vendor/datatables/plugins/bootstrap/dataTables.bootstrap.css',
          '../vendor/jquery-nestable/jquery.nestable.css',
        ], './public/lte/css/lte.css')
      .scripts([
          // base template
          '../vendor/jquery.min.js',
          '../vendor/bootstrap/js/bootstrap.min.js', 
          '../vendor/lte/app.min.js', 
          '../vendor/jquery-slimscroll/jquery.slimscroll.min.js',
          // '../vendor/lte/plugins/fastclick/fastclick.min.js', 
          '../vendor/datatables/media/js/jquery.dataTables.min.js',
          '../vendor/datatables/plugins/bootstrap/dataTables.bootstrap.js',
          '../vendor/bootstrap-datepicker/js/bootstrap-datepicker.js',
          '../vendor/jquery/plugins/sortelements/jquery.sortelements.js', 
          '../vendor/jquery-ui/jquery-ui.min.js',
          '../vendor/jquery-nestable/jquery.nestable.js',
          '../vendor/iCheck/icheck.min.js',
        ], './public/lte/js/lte.js');

```

Then add this to mix.version:
```js
        'public/lte/css/lte.css',
        'public/lte/js/lte.js',
```

### Set up Node for Elixir builds

Charabanc is set up to build the required Javascript and CSS files from source using Elixir.

Install [Node JS](https://nodejs.org/en/) on your host machine. Please note, that it is often easier to build this on the host machine rather than inside your virtual machine, due to compatibility issues with Centos 6.

Install NPM packages (you may need to use sudo):

```sh
$ npm install
```

The builds are defined in gulpfile.js in the root, and reference the resources/assets folder (which you unzipped above).

To build the assets, simply type:

```sh
$ gulp
```
