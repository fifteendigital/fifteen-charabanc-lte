

@if(Session::has('message'))
<div class="alert alert-info">
    <p><i class="fa fa-info-circle"></i> {{ Session::get('message') }}</p>
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger">
    <p><i class="fa fa-exclamation-triangle"></i> {{ Session::get('error') }}</p>
</div>
@endif

@if ($errors->count())
<div class="alert alert-danger">
    <p><i class="fa fa-exclamation-triangle"></i> There was a problem performing this action. Please check and try again.</p>
    @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
    @endforeach
</div>
@endif
