<a href="{{ url('') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <img src="{{ url('') }}/img/cube_small.png" alt="logo" />
    <span class="app-title">Connect</span>
</a>

<!-- Header Navbar -->
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="hidden-xs">{{ Sentry::getUser()->first_name }} {{ Sentry::getUser()->last_name }} </span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="{{ route('users.edit', Sentry::getUser()->id) }}">
                        <i class="fa fa-user"></i> My Profile</a>
                    </li>
                    <li>
                        <a href="{{ route('logout') }}">
                        <i class="fa fa-key"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
