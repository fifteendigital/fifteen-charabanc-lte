@if ($item->hasChildren())
    <li class="treeview @if ($item->isActive()) active @endif
                        @if ($i == 0 and $item->level == 0) start @endif">
        <a href="{{ $item->getUrl() }}">
            @if ($item->icon)
                <i class="fa {{ $item->icon }}"></i>
            @endif   
            <span>
                {{$item->title}}
            </span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        
        <ul class="treeview-menu">
            @foreach ($item->children as $i => $item)
                @include('Template::layouts.partials.nav')
            @endforeach
        </ul>
    </li>
        
    @else
        <li>
            <a href="{{ $item->getUrl() }}">
                @if ($item->icon)
                    <i class="fa {{ $item->icon }}"></i>
                @endif
                <span>
                    {{ $item->title }}
                </span>
            </a>
        </li>
@endif
