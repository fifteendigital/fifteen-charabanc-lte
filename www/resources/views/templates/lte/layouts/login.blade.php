<!doctype html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
	<!-- BEGIN HEAD -->
	<head>
		{!! MetaTags::render() !!}
		@include('Template::layouts.partials.header')
		@yield('header_custom')
		@stack('styles')

	</head>
	<!-- END HEAD -->

	<body class="login-page">

		@include('Template::layouts.partials.messages')

		@yield('content')

		<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
		@include('Template::layouts.partials.footer')

		@stack('scripts')

	</body>
	<!-- END BODY -->
	
</html>