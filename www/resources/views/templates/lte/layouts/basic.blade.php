<!doctype html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
        <!-- BEGIN HEAD -->
    <head>
        {!! MetaTags::render() !!}
        @include('Template::layouts.partials.header')
        @yield('header_custom')
        @stack('styles')
    </head>

    <body class="skin-black-light sidebar-mini">
        <div class="wrapper">
            <header class="main-header page-logo">
                <!-- BEGIN HEADER -->
                @include('Template::layouts.partials.top-basic')
                <!-- END HEADER -->
                <!-- Logo -->
            </header>

            <div class="content-wrapper">
                <section class="content">
                    @yield('content')
                </section>

            </div>

            <!-- BEGIN FOOTER -->
            @include('Template::layouts.partials.bottom')
            <!-- END FOOTER -->

            <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
            @include('Template::layouts.partials.footer')

            <!-- END JAVASCRIPTS -->
            @stack('scripts')
        </div>
    </body>

</html>
