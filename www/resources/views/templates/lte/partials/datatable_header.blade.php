<div class="row hidden-print">
	<div class="col-md-6 col-sm-12">
		<div class="dataTables_length">
			{!! Form::open(['method' => 'GET', 'class' => 'form-inline']) !!}
			<label>
				{!! Form::select('records', $data_table->getRecordsPerPageOptions(), $data_table->getRecordsPerPage(), ['size' => 1, 'class' => 'form-control input-xsmall form-inline']) !!}
				<span class="">{{ strtolower(Alang::get('general.records')) }}</span>
			</label>
				
			{!! Form::close() !!}
		</div>
	</div>
	<div class="col-md-6 col-sm-12">
		<div class="dataTables_filter pull-right">
			{!! Form::open(['method' => 'GET']) !!}
			<label>
				{{ Alang::get('general.search') }}: 
				{!! Form::text('search', $data_table->getSearchString(), ['size' => 1, 'class' => 'form-control input-medium input-inline']) !!}
			</label>
			{!! Form::close() !!}
		</div>
	</div>
</div>