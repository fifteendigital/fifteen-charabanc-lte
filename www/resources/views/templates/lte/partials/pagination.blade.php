
@if ($paginator->getLastPage() > 1)
	<div class="row data_table_controls">
		<div class="col-md-5 col-sm-12">
			<div class="dataTables_info" id="sample_2_info">
				Showing {{ $paginator->getFrom() }} to {{ $paginator->getTo() }} of {{ $paginator->getTotal() }} entries
			</div>
		</div>
		<div class="col-md-7 col-sm-12">
			<div class="dataTables_paginate paging-bootstrap">
				<ul class="pagination">
					<?php echo $presenter->render(); ?>
				</ul>
			</div>
		</div>
	</div>
@endif