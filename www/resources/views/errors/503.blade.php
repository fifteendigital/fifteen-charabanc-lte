@extends('Template::layouts.basic')

@section('content')

<div class="row">
    <div class="col-md-12 page-404">
        <div class="number">
             {{ Alang::get('general.503') }}
        </div>
        <div class="details">
            <h3>{{ Alang::get('general.something_went_wrong') }}.</h3>
            <p>
                {{ Alang::get('general.please_let_us_know_if_you_continue_to_encounter_problems') }}.<br/>
                <a href="{{ route('home') }}">{{ Alang::get('general.return_home') }}</a>
            </p>
        </div>
    </div>
</div>

@endsection

@push('styles')
    {!! Html::style(elixir('metronic/css/error.css')) !!}
@endpush

