@extends('Template::layouts.basic')

@section('content')

<div class="row">
    <div class="col-md-12 page-404">
        <div class="number">
             {{ Alang::get('general.404') }}
        </div>
        <div class="details">
            <h3>{{ Alang::get('general.oops_youre_lost') }}.</h3>
            <p>
                {{ Alang::get('general.we_cannot_find_the_page_youre_looking_for') }}.<br/>
                <a href="{{ route('home') }}">{{ Alang::get('general.return_home') }}</a>
            </p>
        </div>
    </div>
</div>

@endsection

@push('styles')
    {!! Html::style(elixir('metronic/css/error.css')) !!}
@endpush

