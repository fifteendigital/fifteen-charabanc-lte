@extends('Template::layouts.login')

@section('content')

<div class="login-box">
    <div class="login-logo">
    	<img src="{{ url('img/cube_small.png') }}" alt="Logo" />
        <a href="{{ url('/home') }}"><strong>Fifteen</strong>Connect</a>
    </div><!-- /.login-logo -->


    <div class="login-box-body">
	    <p class="login-box-msg">{{ Alang::get('general.enter_any_email_and_password') }}.</p>
	    {!! Form::open(['route' => 'sessions.store', 'class' => 'login-form']) !!}
	        <div class="form-group has-feedback">
	            <input type="email" class="form-control" placeholder="{{ Alang::get('general.email_address') }}" name="email"/>
	        </div>
	        <div class="form-group has-feedback">
	            <input type="password" class="form-control" placeholder="{{ Alang::get('general.password') }}" name="password"/>
	        </div>
	        <div class="row">
	            <div class="col-xs-8">
	            </div><!-- /.col -->
	            <div class="col-xs-4">
	                <button type="submit" class="btn btn-primary btn-block btn-flat">{{ Alang::get('general.login') }}</button>
	            </div><!-- /.col -->
	        </div>
	    {!! Form::close() !!}

	</div><!-- /.login-box-body -->

</div><!-- /.login-box -->