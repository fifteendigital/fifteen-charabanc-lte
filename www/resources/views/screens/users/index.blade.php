@extends('Template::layouts.master')

@section('content')

	<div class="row">
		<div class="col-md-6">
			<h2>{{ Alang::get('general.users') }}</h2>
		</div>

		<div class="col-md-6 text-right">
			<br />
			<a class="btn btn-primary" href="{{ route('users.create') }}">
				<i class="fa fa-plus"></i> {{ Alang::get('general.add_new') }}
			</a>
		</div>
	</div>

	<div class="box">
		<div class="box-body">

			@include('Template::partials.datatable_header')

			@if ($records->count())
				<table class="table table-bordered table-striped dataTable datatables">
					<thead>
						<tr class="sort-header">
							{!! $data_table->sortBy('last_name', Alang::get('general.name')) !!}
							{!! $data_table->sortBy('email', Alang::get('general.email')) !!}
							<th>{{ Alang::get('general.groups') }}</th>
							<th class="text-center">{{ Alang::get('general.actions') }}</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($records as $i => $record)
							<tr>
								<td>{{ $record->first_name }} {{ $record->last_name }}</td>
								<td>{{ $record->email }}</td>
								<td>
									<?php $last_item = count($record->getGroups()) - 1; ?>
									@foreach ($record->getGroups() as $i => $group)
										{{ $group->name }}@if ($i != $last_item),@endif
									@endforeach
								</td>
								<td class="text-center">
									<a class="btn btn-default btn-xs" 
										href="{{ route('users.show', $record->id) }}" 
										title="{{ Alang::get('general.view_record') }}">
										<i class="fa fa-search"></i>
										{{ Alang::get('general.view') }}
									</a>
									&ensp;
									<a class="btn btn-default btn-xs" 
										href="{{ route('users.edit', $record->id) }}"
										title="{{ Alang::get('general.edit_record') }}">
										<i class="fa fa-pencil"></i>
										{{ Alang::get('general.edit') }}
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				@include('Template::partials.datatable_footer')
				
				<div class="clearfix"></div>
			@else
				<p>
					{{ Alang::get('general.there_are_currently_no_records') }}. 
					<a href="{{ route('users.create') }}">{{ Alang::get('general.create_a_new_record') }}</a>.
				</p>
			@endif
		</div>
	</div>

@endsection
