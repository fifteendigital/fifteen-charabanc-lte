<div class="modal fade" id="confirm_delete" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><i class="fa fa-exclamation-triangle"></i> {{ Alang::get('general.delete_record') }}?</h4>
			</div>
			<div class="modal-body">
				<p>{{ Alang::get('general.are_you_sure_you_would_like_to_delete_this_record') }}?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">
					<i class="fa fa-times"></i> {{ Alang::get('general.cancel') }}
				</button>

				{!! Form::open(['route' => ['users.destroy', $record->id], 'method' => 'delete', 'class' => 'inline']) !!}
					<button type="submit" class="red btn btn-warning">
						<i class="fa fa-trash-o"></i> {{ Alang::get('general.delete') }}
					</button>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>