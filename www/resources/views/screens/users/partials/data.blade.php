
<div class="form-group">
	<label class="col-md-3 control-label">{{ Alang::get('general.first_name') }}:</label>
	<div class="col-md-9">
		<p class="form-control-static">{{ $record->first_name }}</p>
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label">{{ Alang::get('general.last_name') }}:</label>
	<div class="col-md-9">
		<p class="form-control-static">{{ $record->last_name }}</p>
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label">{{ Alang::get('general.email') }}:</label>
	<div class="col-md-9">
		<p class="form-control-static">{{ $record->email }}</p>
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label">{{ Alang::get('general.groups') }}:</label>
	<div class="col-md-9">
		@foreach ($record->getGroups() as $group)
			<p class="form-control-static">{{ $group->name }}</p>
		@endforeach
	</div>
</div>

@if ( ! empty($record->id) && $record->last_login != '')
	<div class="form-group">
		<label class="col-md-3 control-label">{{ Alang::get('general.last_login') }}:</label>
		<div class="col-md-9">
			<p class="form-control-static">{{ $record->last_login->format('d/m/Y H:i') }}</p>
		</div>
	</div>
@endif
