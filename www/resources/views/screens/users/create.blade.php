@extends('Template::layouts.master')

@section('content')

	<div class="row">
		<div class="col-md-6">
			<h2>{{ Alang::get('general.new_user') }}</h2>
		</div>
	</div>

	<div class="box">
			
		<div class="box-body">

			{!! Form::open(['route' => 'users.store', 'role' => 'form', 'class' => 'form-horizontal']) !!}
				
				<div class="form-body">

					@include('screens.users.partials.form')
					
					<p>
						<em>* {{ Alang::get('general.required_fields') }}</em>
					</p>
				</div>

				<div class="form-actions">
					<div class="col-md-offset-3 col-md-9">
						<a class="btn btn-default" href="{{ route('users.index') }}">{{ Alang::get('general.back') }}</a>
						{!! Form::submit(Alang::get('general.save'), ['class' => 'btn btn-primary']) !!}
					</div>
				</div>

			{!! Form::close() !!}

		</div>

	</div>

@endsection