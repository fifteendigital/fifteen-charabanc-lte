@extends('Template::layouts.master')

@section('page_title')

	<h3 class="page-title">{{ Alang::get('general.sort_users') }}</h3>

@endsection

@section('content')

	<div class="portlet box blue-hoki">
		<div class="portlet-title">
			<div class="caption">
				{{ Alang::get('general.sort_users') }}
			</div>
		</div>
		<div class='portlet-body dd sortabl'>
			
			<p>{{ Alang::get('general.click_and_drag_the_items_to_change_the_sort_order') }}.</p>

			{!! Form::open(['route' => 'users.sequence', 'role' => 'form']) !!}

				<ol class="dd-list">
					@foreach ($records->orderBy('sequence')->get() as $record)
						<li class="dd-item">
							{!! Form::hidden('ids[]', $record->id) !!}
							<div class="dd-handle">{{ $record->email }}</div>
						</li>
					@endforeach
				</ol>
				<div class="text-right">
					<a class="btn default" href="{{ route('users.index') }}"><i class="fa fa-times"></i> {{ Alang::get('general.cancel') }}</a>
					<button class="btn green">
						<i class="fa fa-check"></i> {{ Alang::get('general.save') }}</a>
					</button>
				</div>

			{!! Form::close() !!}

			<div class="clearfix"></div>
		</div>
	</div>

@endsection

