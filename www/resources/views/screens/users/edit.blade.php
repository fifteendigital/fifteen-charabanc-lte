@extends('Template::layouts.master')

@section('content')

	<div class="row">
		<div class="col-md-6">
			<h2>{{ Alang::get('general.edit_record') }}</h2>
		</div>

		<div class="col-md-6 text-right">
			<br />
			@if (Sentry::getUser()->hasAccess('users.all'))
				<a href="#confirm_delete" class="btn btn-danger" role="button" data-toggle="modal">
					<i class="fa fa-trash-o"></i> {{ Alang::get('general.delete') }}
				</a>
			@endif
		</div>
	</div>

	<div class="box">
		<div class="box-body">

			{!! Form::model($record, ['method' => 'PATCH', 'route' => ['users.update', $record->id], 'role' => 'form', 'class' => 'form-horizontal']) !!}
				
				<div class="form-body">

					@include('screens.users.partials.form')

					<p>
						<em>* {{ Alang::get('general.required_fields') }}</em>
					</p>
				</div>

				<div class="form-actions">
					<div class="col-md-offset-3 col-md-9">
						@if (Sentry::getUser()->hasAccess('users.all'))
							<a class="btn btn-default" href="{{ route('users.show', $record->id) }}">
								{{ Alang::get('general.cancel') }}
							</a>
						@endif
						{!! Form::submit(Alang::get('general.save'), ['class' => 'btn btn-primary']) !!}
					</div>
				</div>

			{!! Form::close() !!}

		</div>
		
	</div>

	@if (Sentry::getUser()->hasAccess('users.all'))
		@include('screens.users.partials.delete')
	@endif

@endsection