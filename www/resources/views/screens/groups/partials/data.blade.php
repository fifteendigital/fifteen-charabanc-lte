
<div class="form-group">
	<label class="col-md-3 control-label">{{ Alang::get('general.name') }}:</label>
	<div class="col-md-9">
		<p class="form-control-static">{{ $record->name }}</p>
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label">{{ Alang::get('general.permissions') }}:</label>
	<div class="col-md-9">
		@include('screens.groups.partials.show_permissions')
	</div>
</div>