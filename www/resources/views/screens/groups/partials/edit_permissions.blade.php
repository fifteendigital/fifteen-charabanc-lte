
{!! Form::hidden('permissions[menu.admin]', 0) !!}
<div class="checkbox">
	{!! Form::checkbox('permissions[menu.admin]', 
		1, 
		( ! empty($record) && $record->hasAccess('menu.admin')) ? 1 : 0) !!}
	{{ Alang::get('general.access_to_admin_menu') }}
</div>

{!! Form::hidden('permissions[groups.all]', 0) !!}
<div class="checkbox">
	{!! Form::checkbox('permissions[groups.all]', 
		1, 
		( ! empty($record) && $record->hasAccess('groups.all')) ? 1 : 0) !!}
	{{ Alang::get('general.manage_user_groups') }}
</div>

{!! Form::hidden('permissions[users.all]', 0) !!}
<div class="checkbox">
	{!! Form::checkbox('permissions[users.all]', 
		1, 
		( ! empty($record) && $record->hasAccess('users.all')) ? 1 : 0) !!}
	{{ Alang::get('general.manage_user_accounts') }}
</div>
