
<div class="form-group @if ($errors->first('name')) has-error @endif">
	<label class="col-md-3 control-label">{{ Alang::get('general.name') }}<span class="required">*</span>:</label>
	<div class="col-md-9">
		{!! Form::text('name', null, ['class' => 'form-control']) !!}
		{{ $errors->first('name', '<span class="help-block">:message</span>') }}
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label">{{ Alang::get('general.permissions') }}:</label>
	<div class="col-md-9 checkbox-list">
		@include('screens.groups.partials.edit_permissions')
	</div>
</div>