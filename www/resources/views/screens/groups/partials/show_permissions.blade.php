
<p class="form-control-static">
	@if( ! empty($record) && $record->hasAccess('menu.admin'))
		<i class="fa fa-check"></i>
	@else
		<i class="fa fa-times"></i>
	@endif
	{{ Alang::get('general.access_to_admin_menu') }}
</p>
<br />

<p class="form-control-static">
	@if( ! empty($record) && $record->hasAccess('groups.all'))
		<i class="fa fa-check"></i>
	@else
		<i class="fa fa-times"></i>
	@endif
	{{ Alang::get('general.manage_user_groups') }}
</p>
<br />

<p class="form-control-static">
	@if( ! empty($record) && $record->hasAccess('users.all'))
		<i class="fa fa-check"></i>
	@else
		<i class="fa fa-times"></i>
	@endif
	{{ Alang::get('general.manage_user_accounts') }}
</p>
