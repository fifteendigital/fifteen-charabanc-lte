@extends('Template::layouts.master')

@section('content')

	<div class="row">
		<div class="col-md-6">
			<h2>{{ Alang::get('general.edit_record') }}</h2>
		</div>

		<div class="col-md-6 text-right">
			<br />
			<a href="#confirm_delete" class="btn btn-danger" role="button" data-toggle="modal">
				<i class="fa fa-trash-o"></i> {{ Alang::get('general.delete') }}
			</a>
		</div>
	</div>

	<div class="box">
		<div class="box-body">

			{!! Form::model($record, ['method' => 'PATCH', 'route' => ['groups.update', $record->id], 'role' => 'form', 'class' => 'form-horizontal']) !!}
				
				<div class="form-body">

					@include('screens.groups.partials.form')

					<p>
						<em>* {{ Alang::get('general.required_fields') }}</em>
					</p>
				</div>

				<div class="form-actions">
					<div class="col-md-offset-3 col-md-9">
						<a class="btn btn-default" href="{{ route('groups.show', $record->id) }}">
							{{ Alang::get('general.cancel') }}
						</a>
						{!! Form::submit(Alang::get('general.save'), ['class' => 'btn btn-primary']) !!}
					</div>
				</div>

			{!! Form::close() !!}

		</div>
		
	</div>

	@include('screens.groups.partials.delete')

@endsection