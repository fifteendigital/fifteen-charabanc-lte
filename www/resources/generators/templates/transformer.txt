<?php 

namespace App\Transformers;

use Fifteen\Charabanc\Transformers\BaseTransformer;

class $STUDLY_PLURAL$Transformer extends BaseTransformer {

    /**
    * Transform a collection of items
    *
    * @param $items
    * @return array
    */
    public function transform($record, $detail = false)
    {
        return [
            $FIELDS$
        ];
    }
}