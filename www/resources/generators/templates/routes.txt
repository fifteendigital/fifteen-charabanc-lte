<?php

Route::get('$ROUTE_PATH$',  [
    'uses' => '$STUDLY_PLURAL$Controller@index',
    'as' => '$ROUTE_NAME_PATH$.index'
]);

Route::get('$ROUTE_PATH$/create',   [
    'uses' => '$STUDLY_PLURAL$Controller@create',
    'as' => '$ROUTE_NAME_PATH$.create'
]);

Route::get('$ROUTE_PATH$/sort',     [
    'uses' => '$STUDLY_PLURAL$Controller@sort',
    'as' => '$ROUTE_NAME_PATH$.sort'
]);

Route::get('$ROUTE_PATH$/export',    [
    'uses' => '$STUDLY_PLURAL$Controller@export',
    'as' => '$ROUTE_NAME_PATH$.export'
]);

Route::get('$ROUTE_PATH$/{id}',     [
    'uses' => '$STUDLY_PLURAL$Controller@show',
    'as' => '$ROUTE_NAME_PATH$.show'
]);

Route::get('$ROUTE_PATH$/{id}/edit',    [
    'uses' => '$STUDLY_PLURAL$Controller@edit',
    'as' => '$ROUTE_NAME_PATH$.edit'
]);

Route::post('$ROUTE_PATH$',     [
    'uses' => '$STUDLY_PLURAL$Controller@store',
    'as' => '$ROUTE_NAME_PATH$.store'
]);

Route::post('$ROUTE_PATH$/sequence',    [
    'uses' => '$STUDLY_PLURAL$Controller@sequence',
    'as' => '$ROUTE_NAME_PATH$.sequence'
]);

Route::patch('$ROUTE_PATH$/{id}',   [
    'uses' => '$STUDLY_PLURAL$Controller@update',
    'as' => '$ROUTE_NAME_PATH$.update'
]);

Route::delete('$ROUTE_PATH$/{id}',  [
    'uses' => '$STUDLY_PLURAL$Controller@destroy',
    'as' => '$ROUTE_NAME_PATH$.destroy'
]);