@extends('Template::layouts.master')

@section('content')

    <div class="row">
        <div class="col-md-6">
            <h2>{{ Alang::get('general.view_record') }}</h2>
        </div>

        <div class="col-md-6 text-right">
            <br />
            <a class="btn btn-primary" href="{{ route($EDIT_LINK$) }}">
                <i class="fa fa-pencil"></i> {{ Alang::get('general.edit') }}
            </a>
        </div>
    </div>

    <div class="box">
        <div class="box-body">
            <form class="form-horizontal">
                
                <div class="form-body">

                    @include('screens.$SNAKE_PLURAL$.partials.data')

                </div>

                <div class="form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        <a class="btn btn-default" href="{{ route($REDIRECT_LINK$) }}">
                            {{ Alang::get('general.back') }}
                        </a>
                    </div>
                </div>

            </form>

        </div>
        
    </div>
    $NESTED_ITEMS$

@endsection