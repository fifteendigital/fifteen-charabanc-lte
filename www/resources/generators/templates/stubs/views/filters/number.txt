
<div class="form-group @if ($errors->first('$NAME$')) has-error @endif">
    <label class="col-md-3 control-label">{{ Alang::get('$LANG_APP$.$LABEL$') }}$REQUIRED_STUB$:</label>
    <div class="col-md-9">
        {!! Form::text('$NAME$', isset($filters['$NAME$']) ? $filters['$NAME$'] : null, ['class' => 'form-control']) !!}
        {!! $errors->first('$NAME$', '<span class="help-block">:message</span>') !!}
    </div>
</div>
