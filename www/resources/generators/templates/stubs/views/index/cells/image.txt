
                                    <td class="text-center">
                                        @if (!empty($record->$NAME$))
                                            <a href="{{ url('/images/' . $record->$NAME$) }}" target="_blank">
                                                <img src="{{ url('/thumb/fit/200x50/' . $record->$NAME$) }}" alt="{{ basename($record->$NAME$) }}" />
                                            </a>
                                        @endif
                                    </td>